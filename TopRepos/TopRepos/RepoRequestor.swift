//
//  RepoRequestor.swift
//  TopRepos
//
//  Created by Roberto Sampaio on 08/08/16.
//  Copyright © 2016 Roberto Sampaio. All rights reserved.
//

import Alamofire

class RepoRequestor {
    
    let alamoFireManager = ConnectionManager().configuredAlamofireManager()
    
    func requestRepositoriesWithInitialPage(initialPage : Int,
                                            sucessCompletion: RepoResponse -> Void,
                                            failCompletion : Void -> Void,
                                            commonCompletion: Void -> Void){
        
        alamoFireManager.request(.GET, urlRepoWithInitialPage(initialPage))
            .responseJSON { response in
                if let JSON = response.result.value {
                    do {
                        let repoResponse = try MTLJSONAdapter.modelOfClass(RepoResponse.self, fromJSONDictionary: JSON as! [NSObject : AnyObject])
                        sucessCompletion(repoResponse as! RepoResponse)
                    }
                    catch{
                        failCompletion()
                    }
                }
                else{
                    failCompletion()
                }
                commonCompletion()
                
        }
    }
    
    func urlRepoWithInitialPage(initialPage : Int) -> String{
        return "\(K.Strings.URL.GitSearchJavaReposSortedByStars)\(K.Strings.URL.Params.Page)\(initialPage)"
    }
    
}
