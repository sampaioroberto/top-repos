//
//  PRRequestor.swift
//  TopRepos
//
//  Created by Roberto Sampaio on 09/08/16.
//  Copyright © 2016 Roberto Sampaio. All rights reserved.
//

import Alamofire

class PRRequestor {
    
    let alamoFireManager = ConnectionManager().configuredAlamofireManager()
    
    func requestPullRequestsWithUsername(username : String, repositoryName : String,
                                         successCompletion: [PullRequest] -> Void,
                                        failureCompletion: Void -> Void,
                                        commonCompletion: Void -> Void){
        
        alamoFireManager.request(.GET, urlPullRequestWithUsername(username, repositoryName: repositoryName))
            .responseJSON { response in
                if let JSON = response.result.value {
                    do {
                        if JSON .isKindOfClass(NSArray.self){
                            let pullRequests = try MTLJSONAdapter.modelsOfClass(PullRequest.self, fromJSONArray: JSON as! [AnyObject])
                            successCompletion(pullRequests as! [PullRequest])
                        } else{
                            failureCompletion()
                        }
                    }
                    catch{
                        failureCompletion()
                    }
                } else{
                    failureCompletion()
                }
                commonCompletion()
                
        }
    }
    
    func urlPullRequestWithUsername(username : String, repositoryName : String) -> String{
        var urlPullRequest = K.Strings.URL.GitRepos
        
        urlPullRequest += "\(username)/\(repositoryName)\(K.Strings.URL.Pulls)"
        
        return urlPullRequest
    }

}
