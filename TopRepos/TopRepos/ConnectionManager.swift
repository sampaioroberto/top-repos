//
//  ConnectionManager.swift
//  TopRepos
//
//  Created by Roberto Sampaio on 14/08/16.
//  Copyright © 2016 Roberto Sampaio. All rights reserved.
//

import Alamofire

class ConnectionManager {

    func configuredAlamofireManager() -> Manager{
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.timeoutIntervalForRequest = 4
        configuration.timeoutIntervalForResource = 4
        return Alamofire.Manager(configuration: configuration)
    }
    
}
