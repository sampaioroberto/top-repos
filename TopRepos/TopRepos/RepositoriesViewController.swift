//
//  RepositoriesTableViewController.swift
//  TopRepos
//
//  Created by Roberto Sampaio on 08/08/16.
//  Copyright © 2016 Roberto Sampaio. All rights reserved.
//

import UIKit

class RepositoriesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {

    var repositories :[Repository] = []
    var currentPage = 1
    var selectedRepository = 0
    
    var repoRequestor = RepoRequestor()
    
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addBottomRefresh()
        
        configureTableView()
        configureEmptyDataSet()
        
        requestRepositories()
    }

    // MARK: - Table view data source

    func configureTableView(){
        self.tableView.estimatedRowHeight = 140
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let reuseIdentifier = K.Strings.CellNames.RepoCell
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier, forIndexPath: indexPath) as! RepositoryTableViewCell
        
        let repository = repositories[indexPath.row]
        
        cell.repoName.text = repository.repoName
        cell.repoDescription.text = repository.repoDescription
        cell.forkCount.text = String(repository.forkCount)
        cell.starCount.text = String(repository.starCount)
        cell.username.text = repository.username

        let url = NSURL(string: repository.profilePictureUrl)
        cell.profilePicture.sd_setImageWithURL(url, placeholderImage: UIImage(named: K.Images.Profile))
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedRepository = indexPath.row
        self.performSegueWithIdentifier(K.Segues.ToPullRequest, sender: self)
    }
    
    // MARK: - Empty Data Set
    
    func configureEmptyDataSet(){
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        
        tableView.tableFooterView = UIView()
    }
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: K.Images.EmptyState)
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let title = K.Strings.NoRepoFound
        return NSAttributedString(string: title)
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let title = K.Strings.CheckYourConnection
        return NSAttributedString(string: title)
    }
    
    func buttonTitleForEmptyDataSet(scrollView: UIScrollView!, forState state: UIControlState) -> NSAttributedString! {
        let buttonTitle = K.Strings.TryAgain
        return NSAttributedString(string: buttonTitle)
    }
    
    func emptyDataSetDidTapButton(scrollView: UIScrollView!) {
        showTableView(false)
        requestRepositories()
    }
    
    // MARK: - Request
    
    func requestRepositories(){
        repoRequestor.requestRepositoriesWithInitialPage(currentPage, sucessCompletion: {
            response in
            let repoReponse = response as RepoResponse
            self.repositories += repoReponse.repositories!
        }, failCompletion: {
            //TODO: show specific error message
        }, commonCompletion: {
            self.tableView.reloadData()
            self.addBottomRefresh()
            self.showTableView(true)
        })
    }
    
    func showTableView(show: Bool){
        self.loadingView.hidden = show
        self.tableView.hidden = !show
    }
    
    // MARK: - Bottom Refresh

    func addBottomRefresh(){
        let refreshControl = UIRefreshControl()
        refreshControl.triggerVerticalOffset = 100.0
        refreshControl.addTarget(self, action: #selector(RepositoriesViewController.refresh), forControlEvents: .ValueChanged)
        self.tableView.bottomRefreshControl = refreshControl;
    }
    
    func refresh(){
        currentPage += 1
        requestRepositories()
    }
    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let pullRequestTableViewController = segue.destinationViewController as! PullRequestsViewController
        pullRequestTableViewController.username = repositories[selectedRepository].username
        pullRequestTableViewController.repositoryName = repositories[selectedRepository].repoName
        pullRequestTableViewController.profilePictureUrl = repositories[selectedRepository].profilePictureUrl
    }

}
