//
//  PullRequestsTableViewController.swift
//  TopRepos
//
//  Created by Roberto Sampaio on 09/08/16.
//  Copyright © 2016 Roberto Sampaio. All rights reserved.
//

import UIKit

class PullRequestsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {

    var username : String!
    var repositoryName : String!
    var profilePictureUrl : String!
    
    let prRequestor = PRRequestor()
    
    var descriptionEmptyState = K.Strings.CheckYourConnection
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var loadingView: UIView!
    var pullRequests : [PullRequest] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = repositoryName
        
        configureTableView()
        configureEmptyDataSet()
        
        requestPullRequests()
    }

    // MARK: - Table view data source

    func configureTableView(){
        self.tableView.estimatedRowHeight = 95
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pullRequests.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let reuseIdentifier = K.Strings.CellNames.PRCell
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier, forIndexPath: indexPath) as! PullRequestTableViewCell
        
        let pullRequest = pullRequests[indexPath.row]
        
        cell.username.text = pullRequest.prUsername
        cell.profilePicture.sd_setImageWithURL(NSURL(string: pullRequest.prProfilePictureUrl), placeholderImage: UIImage(named: K.Images.Profile))
        cell.prTitle.text = pullRequest.prTitle
        cell.prBody.text = pullRequest.prBody
        
        let dateFormat = NSDateFormatter()
        dateFormat.dateFormat = K.Strings.DateFormat
        cell.prDate.text = dateFormat.stringFromDate(pullRequest.prDate)

        return cell
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let pullRequest = pullRequests[indexPath.row]
        UIApplication.sharedApplication().openURL(NSURL(string:pullRequest.htmlUrl)!)
    }
    
    // MARK : Empty Data Set
    
    func configureEmptyDataSet(){
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        
        tableView.tableFooterView = UIView()
    }
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: K.Images.EmptyState)
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let title = K.Strings.NoPullRequestFound
        return NSAttributedString(string: title)
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: descriptionEmptyState)
    }
    
    func buttonTitleForEmptyDataSet(scrollView: UIScrollView!, forState state: UIControlState) -> NSAttributedString! {
        let buttonTitle = K.Strings.TryAgain
        return NSAttributedString(string: buttonTitle)
    }
    
    func emptyDataSetDidTapButton(scrollView: UIScrollView!) {
        self.tableView.hidden = true
        self.loadingView.hidden = false
        requestPullRequests()
    }

    
    // MARK : Request
    
    func requestPullRequests(){
        prRequestor.requestPullRequestsWithUsername(username, repositoryName: repositoryName, successCompletion: {
            response in
            self.pullRequests += response
            self.descriptionEmptyState = K.Strings.NoPullRequestInThisRepo
            },
        failureCompletion: {
            //TODO: show specific error message
        },
        commonCompletion: {
            self.tableView.reloadData()
            self.tableView.hidden = false
            self.loadingView.hidden = true
        })
    }
    
}
