//
//  PullRequest.swift
//  TopRepos
//
//  Created by Roberto Sampaio on 09/08/16.
//  Copyright © 2016 Roberto Sampaio. All rights reserved.
//

import UIKit

class PullRequest: MTLModel, MTLJSONSerializing {

    var prTitle = ""
    var prBody = ""
    var prDate = NSDate()
    var prUsername = ""
    var prProfilePictureUrl = ""
    var htmlUrl = ""
    
    static func JSONKeyPathsByPropertyKey() -> [NSObject : AnyObject]!
    {
        return ["prTitle" : "title",
                "prBody" : "body",
                "prDate" : "created_at",
                "prUsername" : "user.login",
                "prProfilePictureUrl" : "user.avatar_url",
                "htmlUrl" : "html_url"]
    }
    
    static func prDateJSONTransformer() -> NSValueTransformer!
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ssZZZ"
        
        let _forwardBlock: MTLValueTransformerBlock? = { (value, success, error) in
            return dateFormatter.dateFromString(value as! String!)
        }
        let _reverseBlock: MTLValueTransformerBlock? = { (value, success, error) in
            return dateFormatter.stringFromDate(value as! NSDate!)
        }
        return MTLValueTransformer(usingForwardBlock:_forwardBlock, reverseBlock:_reverseBlock)
    }

}
