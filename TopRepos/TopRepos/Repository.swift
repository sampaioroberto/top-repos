//
//  Repository.swift
//  TopRepos
//
//  Created by Roberto Sampaio on 09/08/16.
//  Copyright © 2016 Roberto Sampaio. All rights reserved.
//

import UIKit

class Repository: MTLModel, MTLJSONSerializing {
    
    var repoName = ""
    var repoDescription = ""
    var forkCount = 0
    var starCount = 0
    var username = ""
    var profilePictureUrl = ""
    
    static func JSONKeyPathsByPropertyKey() -> [NSObject : AnyObject]!
    {
        return ["repoName" : "name",
                "repoDescription" : "description",
                "forkCount" : "forks_count",
                "starCount" : "stargazers_count",
                "username" : "owner.login",
                "profilePictureUrl" : "owner.avatar_url"]
    }

}
