//
//  Constants.swift
//  TopRepos
//
//  Created by Roberto Sampaio on 12/08/16.
//  Copyright © 2016 Roberto Sampaio. All rights reserved.
//

struct K {
    struct Images {
        static let Profile = "profile.png"
        static let EmptyState = "empty_state.png"
    }
    
    struct Segues {
        static let ToPullRequest = "segueToPullRequest"
    }
    
    struct Strings {
        static let NoRepoFound =  NSLocalizedString("No repository found", comment: "")
        static let NoPullRequestFound = NSLocalizedString("No pull requests found", comment: "")
        static let CheckYourConnection = NSLocalizedString("Check your internet connection and try again", comment: "")
        static let TryAgain = NSLocalizedString("Try again", comment: "")
        static let NoPullRequestInThisRepo = NSLocalizedString("There is no pull request in this repository", comment: "")
        
        static let DateFormat = "dd/MM/YYYY"
        
        struct URL {
            static let GitRepos = "https://api.github.com/repos/"
            static let GitSearchJavaReposSortedByStars = "https://api.github.com/search/repositories?q=language:Java&sort=stars"
            static let Pulls = "/pulls"
            
            struct Params {
                static let Page = "&page="
            }
        }
        
        struct CellNames {
            static let RepoCell = "RepoCell"
            static let PRCell = "PRCell"
        }
    }
}
