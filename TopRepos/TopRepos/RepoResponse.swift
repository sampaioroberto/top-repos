//
//  RepoResponse.swift
//  TopRepos
//
//  Created by Roberto Sampaio on 09/08/16.
//  Copyright © 2016 Roberto Sampaio. All rights reserved.
//

import UIKit

class RepoResponse: MTLModel, MTLJSONSerializing {
    
    var repositories:[Repository]?
    
    static func JSONKeyPathsByPropertyKey() -> [NSObject : AnyObject]!
    {
        return ["repositories" : "items"]
    }
    
    static func repositoriesJSONTransformer() -> NSValueTransformer!
    {
        return MTLJSONAdapter.arrayTransformerWithModelClass(Repository)
    }
}
