//
//  FunctionsSpec.swift
//  TopRepos
//
//  Created by Roberto Sampaio on 13/08/16.
//  Copyright © 2016 Roberto Sampaio. All rights reserved.
//

import Quick
import Nimble
@testable import TopRepos

class FunctionsSpec: QuickSpec {

    override func spec() {
        
        it("testFuncUrlPullRequestWithUsername"){
            let gitReposURL = K.Strings.URL.GitRepos
            let username = "username"
            let repositoryName = "repositoryName"
            
            let prRequestor = PRRequestor()
            let url = prRequestor.urlPullRequestWithUsername(username, repositoryName: repositoryName)
            expect(url).to(match("\(gitReposURL)\(username)/\(repositoryName)\(K.Strings.URL.Pulls)"))
        }
        
        it ("testFuncUrlRepoWithInitialPage"){
            let gitSearchUrl = K.Strings.URL.GitSearchJavaReposSortedByStars
            let page = 3
            
            let repoRequestor = RepoRequestor()
            let url = repoRequestor.urlRepoWithInitialPage(page)
            
            /*
            // Esse teste foi feito com o XCTAssertEqual, pois o expect fornecido pelo
             framework aparentemente não funciona quando a String contém "?"
             
             Testar -> expect(url).to(match("\(gitSearchUrl)\(K.String.URL.Param.Page)\(page)"))
            */
            XCTAssertEqual(url, "\(gitSearchUrl)\(K.Strings.URL.Params.Page)\(page)")
        }
        
        
    }
    
}
