//
//  PullRequestsTableViewControllerSpec.swift
//  TopRepos
//
//  Created by Roberto Sampaio on 12/08/16.
//  Copyright © 2016 Roberto Sampaio. All rights reserved.
//

import Quick
import Nimble
@testable import TopRepos

class PullRequestsViewControllerSpec: QuickSpec {
    override func spec() {
        
        var viewController: PullRequestsViewController!
        var cell: UITableViewCell!
        let storyboard = UIStoryboard(name:"Main", bundle: NSBundle.mainBundle())
        
        beforeEach {
            viewController = storyboard.instantiateViewControllerWithIdentifier("PullRequestsViewControllerID") as! PullRequestsViewController
            
            viewController.username = ""
            viewController.repositoryName = ""
            viewController.pullRequests = [PullRequest()]
            
            let _ = viewController.view //Força o carregamento dos IBOutlets
            
            waitUntil{done in
                viewController.tableView.reloadData()
                done()
            }
        }
        
        describe("numberOfSectionsInTableView"){
            
            it("should be 1"){
                expect(viewController.tableView.numberOfSections).to(equal(1))
            }
        }
        
        describe("cellForRowAtIndexPath"){
            
            beforeEach {
                let indexPath = NSIndexPath(forRow: 0, inSection: 0)
                cell = viewController.tableView.cellForRowAtIndexPath(indexPath)
            }
            
            it("should be an instance of PullRequestsTableViewCell"){
                expect(cell).to(beAnInstanceOf(PullRequestTableViewCell))
            }
            
            it("should have RepoCell for reuse identifier"){
                expect(cell.reuseIdentifier).to(equal(K.Strings.CellNames.PRCell))
            }
        }
        
        describe("request pull requests"){
            it("should have pull requests"){
                viewController.pullRequests = []
                viewController.username = "facebook"
                viewController.repositoryName = "react-native"
                dispatch_async(dispatch_get_main_queue()) {
                    viewController.requestPullRequests()
                }
                expect(viewController.tableView.numberOfRowsInSection(0)).toEventually(beGreaterThan(0), timeout : 3)
            }
        }
    }
}
