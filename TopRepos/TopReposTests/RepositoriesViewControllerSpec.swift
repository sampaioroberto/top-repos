//
//  RepositoriesTableViewControllerSpec.swift
//  TopRepos
//
//  Created by Roberto Sampaio on 12/08/16.
//  Copyright © 2016 Roberto Sampaio. All rights reserved.
//

import Quick
import Nimble
@testable import TopRepos

class RepositoriesViewControllerSpec: QuickSpec {
    override func spec() {
        
        var viewController: RepositoriesViewController!
        var tableView: UITableView!
        var cell: UITableViewCell!
        let storyboard = UIStoryboard(name:"Main", bundle: NSBundle.mainBundle())
        let repositories : [Repository] = [Repository()]
        
        beforeEach {
            viewController = storyboard.instantiateViewControllerWithIdentifier("RepositoriesViewControllerID") as! RepositoriesViewController
            viewController.repositories = repositories
            let _ = viewController.view //Força o carregamento dos IBOutlets
            tableView = viewController.tableView
        }
        
        describe("numberOfSectionsInTableView"){
            
            it("should be 1"){
                expect(tableView.numberOfSections).to(equal(1))
            }
        }
        
        describe("cellForRowAtIndexPath"){
            
            beforeEach {
                let indexPath = NSIndexPath(forRow: 0, inSection: 0)
                cell = viewController.tableView(tableView, cellForRowAtIndexPath: indexPath)
            }
            
            it("should be an instance of RepositoryTableViewCell"){
                expect(cell).to(beAnInstanceOf(RepositoryTableViewCell))
            }
            
            it("should have RepoCell for reuse identifier"){
                expect(cell.reuseIdentifier).to(equal(K.Strings.CellNames.RepoCell))
            }
        }
        
        describe("request repositories"){
            it("should have repositories after request"){
                dispatch_async(dispatch_get_main_queue()) {
                    viewController.requestRepositories()
                }
                expect(tableView.numberOfRowsInSection(0)).toEventually(beGreaterThan(1), timeout : 3)
            }
        }
    }
}
